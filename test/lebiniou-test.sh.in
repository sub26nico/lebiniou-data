#!/usr/bin/env bash

function check_result() {
    if [ $? -eq 0 ]; then
        echo "[+] Success."
    else
        echo "[!] Failed."
        exit 1
    fi
}

AUTOPKGTEST_TMP=${AUTOPKGTEST_TMP:-/tmp}
echo "[i] AUTOPKGTEST_TMP= '$AUTOPKGTEST_TMP'"
echo "[i] Setting up environment..."
# Input file
export LEBINIOU_SNDFILE=@prefix@/share/lebiniou/test/EP-Le_cri_des_anges-Intro_4-8bits.flac
# Set random seed
export LEBINIOU_SEED=${LEBINIOU_SEED:-$RANDOM}
echo "[i] Random seed: "$LEBINIOU_SEED
# Log sequences
export LEBINIOU_TEST=1
# Dump frames to disk
export LEBINIOU_DUMP_FRAMES=1
# Copy configuration file
CONFDIR=$AUTOPKGTEST_TMP/.lebiniou
mkdir $CONFDIR 2> /dev/null
CONFIGURATION="@prefix@/share/lebiniou/test/lebiniou.json"
cp $CONFIGURATION $CONFDIR
# cat $CONFDIR/lebiniou.json
OPTIONS="-q -o mp4 -s -w 0 -E -W 0 -B -c ${CONFDIR}/lebiniou.json"
echo "[i] Options: "$OPTIONS
# FFmpeg
# echo "[i] FFmpeg:"
# ffmpeg -version 2>&1
# Version
# echo "[i] Version:"
# lebiniou --version
# check_result
# Help
# echo "[i] Help:"
# lebiniou -q --help
# check_result
# Statistics
# echo "[i] Statistics:"
# lebiniou -q -i twip -o NULL --stats
# check_result
# First video
export LEBINIOU_MP4_FILENAME=$AUTOPKGTEST_TMP/video1.mp4
echo -n "[i] Encoding video #1... "
lebiniou $OPTIONS > $AUTOPKGTEST_TMP/video1.log
echo "done."
# Second video
export LEBINIOU_MP4_FILENAME=$AUTOPKGTEST_TMP/video2.mp4
echo -n "[i] Encoding video #2... "
lebiniou $OPTIONS > $AUTOPKGTEST_TMP/video2.log
echo "done."
# Third video
export LEBINIOU_MP4_FILENAME=$AUTOPKGTEST_TMP/video3.mp4
echo -n "[i] Encoding video #3... "
lebiniou $OPTIONS > $AUTOPKGTEST_TMP/video3.log
echo "done."
echo "[i] Generated videos and logs:"
ls -lh $AUTOPKGTEST_TMP/*.mp4 $AUTOPKGTEST_TMP/*.log
echo "[i] Comparing logs:"
echo -n "    * video1.log: "
sha256sum $AUTOPKGTEST_TMP/video1.log
echo -n "    * video2.log: "
sha256sum $AUTOPKGTEST_TMP/video2.log
echo -n "    * video3.log: "
sha256sum $AUTOPKGTEST_TMP/video3.log
diff $AUTOPKGTEST_TMP/video1.log $AUTOPKGTEST_TMP/video2.log && diff $AUTOPKGTEST_TMP/video2.log $AUTOPKGTEST_TMP/video3.log
check_result
echo "[i] Checking if the galaxy plugin was present:"
for i in 1 2 3; do
    echo -n "    * video$i.log: "
    grep galaxy $AUTOPKGTEST_TMP/video$i.log | wc -l
done
echo "[i] Extracting per-packet SHA256 hashes:"
echo -n "    * video1.mp4... "
ffmpeg -loglevel quiet -i $AUTOPKGTEST_TMP/video1.mp4 -map 0:v -f framehash - > $AUTOPKGTEST_TMP/out1.sha256
echo "done."
echo -n "    * video2.mp4... "
ffmpeg -loglevel quiet -i $AUTOPKGTEST_TMP/video2.mp4 -map 0:v -f framehash - > $AUTOPKGTEST_TMP/out2.sha256
echo "done."
echo -n "    * video3.mp4... "
ffmpeg -loglevel quiet -i $AUTOPKGTEST_TMP/video3.mp4 -map 0:v -f framehash - > $AUTOPKGTEST_TMP/out3.sha256
echo "done."
echo "[i] Generated hashes:"
ls -lh $AUTOPKGTEST_TMP/*.sha256
echo "[i] Comparing per-packet hashes:"
diff $AUTOPKGTEST_TMP/out1.sha256 $AUTOPKGTEST_TMP/out2.sha256 && diff $AUTOPKGTEST_TMP/out2.sha256 $AUTOPKGTEST_TMP/out3.sha256
check_result
echo "[i] Comparing dumped frames:"
sha256sum $AUTOPKGTEST_TMP/video1.mp4-*.png|awk '{ print $1 }' > $AUTOPKGTEST_TMP/frames1.log
sha256sum $AUTOPKGTEST_TMP/video2.mp4-*.png|awk '{ print $1 }' > $AUTOPKGTEST_TMP/frames2.log
sha256sum $AUTOPKGTEST_TMP/video3.mp4-*.png|awk '{ print $1 }' > $AUTOPKGTEST_TMP/frames3.log
diff $AUTOPKGTEST_TMP/frames1.log $AUTOPKGTEST_TMP/frames2.log && diff $AUTOPKGTEST_TMP/frames2.log $AUTOPKGTEST_TMP/frames3.log
check_result
echo -n "[i] Encoding comparison video... "
ffmpeg -loglevel quiet -i $AUTOPKGTEST_TMP/video1.mp4 -i $AUTOPKGTEST_TMP/video2.mp4 -i $AUTOPKGTEST_TMP/video3.mp4 -filter_complex hstack,hstack -c:v libx264 -crf 0 $AUTOPKGTEST_TMP/comparison.mp4
echo "done."
echo "[i] Random seed was: "$LEBINIOU_SEED
